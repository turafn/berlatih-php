<?php
function tentukan_nilai($number)
{
    if ($number <= 100) {
        $tentukan_nilai = "Sangat Baik" . '<br>';
    }
    if ($number <= 85) {
        $tentukan_nilai = "Baik" . '<br>';
    }
    if ($number <=  70) {
        $tentukan_nilai = "Cukup".'<br>';
    } 
    if ($number < 60) {
        $tentukan_nilai = "Kurang".'<br>';
    }
    echo $number.'='.$tentukan_nilai.'<br>';
}

//TEST CASES
echo 'Nilai <br><br>';
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
